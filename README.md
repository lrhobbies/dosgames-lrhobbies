# lrhobbies
This is a repository of all infra-as-code and associated files and data for Legendary Realms. This project was insipired by [em_dosbox](https://github.com/dreamlayers/em-dosbox).

## Installation

Clone the repository with:

```bash
git clone https://gitlab.com/dcassiero/dosgames-lrhobbies.git
```

## Usage

### site
Contains the code for the frontend site as well as the reverse proxy.

## Diagram
(a general idea, subject to change)
https://go.gliffy.com/go/html5/13289187

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Credits
Inspiration: [em_dosbox](https://github.com/dreamlayers/em-dosbox)
Help: 

## License
[MIT](https://choosealicense.com/licenses/mit/)